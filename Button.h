#ifndef BUTTON_H
#define BUTTON_H
#include <Arduino.h>


class Button{
    private:
        unsigned long _start_time;
        bool _has_been_released = false;
        int _buttonPin;
        bool _measure_button_state();
    public:
        Button(int);
        void init();
        bool button_pressed();
};

#endif
