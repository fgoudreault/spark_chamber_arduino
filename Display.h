#ifndef DISPLAY_H
#define DISPLAY_H
#include <U8g2lib.h>
#include <SPI.h>
#include <Wire.h>

class Display{
    private:
        U8G2_SSD1306_128X32_UNIVISION_F_HW_I2C* _u8g2;
        unsigned long _last_logging_time;
        int _loglevel;
        void _display_spark_count_log(int);
        void _display_temperature_log(float);
    public:
        Display(int);
        void init();
        void display_spark_count(int);
        void display_temperature(float);
        void display_error_temperature();
        void display_string(char*);
        float round_1(float);
};
#endif
