#ifndef SPARKCONTROLLER_H
#define SPARKCONTROLLER_H
// #include <Arduino.h>
#include "Button.h"
#include "Display.h"
#include <DHT.h>
#include <DHT_U.h>


class SparkController{
    private:
        int _ledPin;
        int _dhtPin;
        int _mode;
        int _buttonPin;
        int _spark_count;
        int _loglevel;
        unsigned long _last_logging_time;
        Button* _button;
        DHT* _dht;
        Display* _display;
    public:
        SparkController(int, int, int, int);
        void init();
        int get_mode();
        void switch_mode();
        bool button_pressed();
        void update_display();
        void control_chamber();
};

#endif
