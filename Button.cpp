#include "Button.h"


Button::Button(int buttonPin){
  _buttonPin = buttonPin;
}
void Button::init(){
  // nothing really to do
  _start_time = millis();
}
bool Button::_measure_button_state(){
  if (digitalRead(_buttonPin) == HIGH){
    return true;
  }
  return false;
}

bool Button::button_pressed(){
  if (_measure_button_state()){
    // button signal is HIGH
    unsigned long current_time = millis();
    unsigned long elapsed_time = current_time - _start_time;
    if (elapsed_time<1000){
        // need to press button for 1 sec to activate it
        return false;
    }
    else{
        if (_has_been_released){
            // button was released earlier.
            // while the previous iteration was pressed for > 1s
            // the button was pressed correctly
            // reset timer and set the released status to false
            _has_been_released = false;
            _start_time = millis();
            return true;
        }
        else{
            // button was not released since last time it
            // started to be pressed. need to be released to
            // make it count
            return false;
        }
    }
  }
  else{
      // button signal is LOW
      // reset timer
      _start_time = millis();
      _has_been_released = true;
      return false;
  }
}
