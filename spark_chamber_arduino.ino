// for led display
#include <Arduino.h>
#include "SparkController.h"

// my custom variables
int ledState = 0;
int ledPin = 13;
int buttonPin = 2;
int dhtPin = 4;
int loglevel = 0;  // 0 = DEBUG, 1 = INFO, 2 = WARNING, 3 = ERROR

SparkController spark_controller(ledPin, buttonPin, dhtPin, loglevel);

void setup()
{
  Serial.begin(9600);
  // sometimes, setup is called twice when uploading but don't worry
  // this will print initializing twice but it's not a big deal
  spark_controller.init();
}

void loop()
{
  // check if button has been pressed while the 'release' state was false.
  // if so, increase spark count
  // otherwise do nothing else.
  if (spark_controller.button_pressed()){  // startTime, button_has_been_released)) {
    // sparkCount++;
    spark_controller.switch_mode();
  }
  // main part of code, the display will be different
  spark_controller.update_display();
  // control chamber
  spark_controller.control_chamber();
}  // main program END
