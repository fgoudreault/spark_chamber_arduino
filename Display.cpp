#include "Display.h"

Display::Display(int loglevel){
  _loglevel=loglevel;
  _u8g2 = new U8G2_SSD1306_128X32_UNIVISION_F_HW_I2C(U8G2_R0);
}
void Display::_display_spark_count_log(int spark_count){
    // log the spark count without flooding the serial monitor i.e.: 1msg/sec max
  unsigned long current_time = millis();
  if (current_time - _last_logging_time >= 1000 && _loglevel == 0){
    Serial.print("DEBUG: Displaying spark count of: ");
    Serial.println(spark_count);
    _last_logging_time = current_time; 
  }
}
void Display::_display_temperature_log(float temperature){
    // log the temperature without flooding the serial monitor i.e.: 1msg/sec max
  unsigned long current_time = millis();
  if (current_time - _last_logging_time >= 1000 && _loglevel == 0){
    Serial.print("DEBUG: Displaying temperature of: ");
    Serial.println(temperature);
    _last_logging_time = current_time; 
  }
}
void Display::init(){
  _last_logging_time = millis();
  if (_loglevel <= 0){
    Serial.println("INFO: Initializing LED display...");
  }
  _u8g2->begin();
}
void Display::display_spark_count(int spark_count){
  _display_spark_count_log(spark_count);
  char spark_count_string[10];  // string that will be displayed
  sprintf(spark_count_string, "%i", spark_count);
  display_string(spark_count_string);
}
void Display::display_temperature(float temperature){
  _display_temperature_log(temperature);
  // first determine string length to display
  char temperature_string[7];
  dtostrf(temperature, 4, 1, temperature_string);
  temperature_string[5] = "C";
  display_string(temperature_string);  
}
void Display::display_error_temperature(){
  // display error temperature
  char err[5] = "ERR T";
  display_string(err);
}
void Display::display_string(char* string){
  _u8g2->clearBuffer();  // clear internal memory
  _u8g2->setFont(u8g2_font_logisoso28_tr);  // choose a suitable font at https://github.com/olikraus/u8g2/wiki/fntlistall
  // first 2 args: (x,y) position of text, then text to display
  _u8g2->drawStr(8, 29, string);  // write something to the internal memory
  _u8g2->sendBuffer();          // transfer internal memory to the display
}
float Display::round_1(float number){
  // round a number up tp 1 decimal
  // inspired from https://www.geeksforgeeks.org/rounding-floating-point-number-two-decimal-places-c-c/
  // basically, type casting to int the value * 10 will effectively round the number to correct number of digit
  // adding .5 before casting makes sure we round correctly (since casting will just chop decimals)
  // then return this rounded value divided by 10
  float value = (int)(number * 10.f + .5f);
  return (float)value / 10.0f;
}
