#include "SparkController.h"

#define DHTTYPE DHT11

// mode = 0 means print spark count
// mode = 1 show temperature
// mode = 2 show relative humidity

SparkController::SparkController(int ledPin, int buttonPin, int dhtPin, int loglevel){
  // spark controller constructor method
  _spark_count = 0;
  _loglevel = loglevel;
  _ledPin = ledPin;
  _dhtPin = dhtPin;
  _buttonPin = buttonPin;
  _display = new Display(_loglevel);
  _button = new Button(_buttonPin);  
  _dht = new DHT(_dhtPin, DHTTYPE);
  _mode = 1;
}
void SparkController::init(){
  // initialize stuff
  if (_loglevel <= 1){
    Serial.println("Initializing SparkController...");
  }
  pinMode(_ledPin, OUTPUT);
  pinMode(_buttonPin, INPUT);
  _last_logging_time = millis();

  // DHT humidity + temperature sensor stuff
  _dht->begin();
  // LED display stuff
  _display->init();
  // button stuff      
  _button->init();
}
bool SparkController::button_pressed(){
  return _button->button_pressed();
}
int SparkController::get_mode(){
  return _mode;
}
void SparkController::switch_mode(){
  // increment mode by 1 mod 3
  _mode = (_mode + 1) % 3;
  if (_loglevel <= 1){
    Serial.print("INFO: swithing to mode: ");
    Serial.println(_mode);
  }
} 
void SparkController::update_display(){
  switch(_mode){
    case 0:
      _display->display_spark_count(_spark_count);
      break;
    case 1:
      float temperature = _dht->readTemperature();  // already in degrees Celsius
      _display->display_temperature(temperature);
      break;
  }
}
void SparkController::control_chamber(){
}
